FROM node
RUN mkdir /app
COPY . /app 

WORKDIR /app

COPY package.json /app
RUN yarn install

RUN yarn build

EXPOSE 3000

CMD yarn start
